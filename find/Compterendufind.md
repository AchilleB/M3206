## Exerice find

La commande suivante :
find /tmp -name *.txt -ok rm {} \;

Permet d'effacé les fichiers .txt se situant dans le répertoire /tmp avec demande de confirmation " l'option -ok"


La commande cpio sauvegarde sur sa sortie standard tous les fichiers dont elle lit le nom sur son entrée standard.

La commande cp -R permet de copié tout les fichiers d'un répertoire

La commande qui permet de lister les premières lignes de tous les fichiers *.txt d'un répertoire. :

find /rep -name *.txt -exec head -10 {} \;

