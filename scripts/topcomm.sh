HISTFILE =~ ./bash_history
set -o history
history | awk '{print $2}' | sort  | uniq -c | sort -nr | head $


#On charge le fichier history, histfile est le répertoire comportenlle stockage de l'historique de bash. 
