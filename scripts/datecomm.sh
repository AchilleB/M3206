#!/bin/bash
f="first"
l="last"

if [ $1 = $f ]	
then
time=$(cat $3 | awk '{print $2}' | grep $2| cut -d ':' -f1 | head -1 )
fi

if [ $1 = $l ]
then
time=$(cat $3 | awk '{print $2}' | grep $2| cut -d ':' -f1 | tail -1 )
fi


if [ $1 != $f ] && [ $1 != $l ] 
then
echo "First argument should be first or last."
echo " usage : ./datecommprof.sh [last|first] [command] [file]"
fi 




if [ ! -e $3 ] #Si le fichier ecrit en paramètre est incorrect ( -e verifie si fichier exisste )
then
echo " Le fichier "$3" n'existe pas " 
echo " usage : ./datecommprof.sh [last|first] [command] [file]"
fi


#existe=$( cat /root/M3206/scripts/my_history | awk '{print$2}' | grep $2 | wc -l) # 
#Variable pour verifier que la commande passer en parametre existe

#if [ $existe -eq 0 ] #si il n'y a pas d'occurence
#then
#echo " Cette commande n'apparait pas dans my_history "
#fin





date -d @$time


#on créer une variable time , qui prend en compte les paramètres que l'on rentre en autre la commande et le fichier ici history

#l'option date permet d'affiché la date au format string (-d)


#Ici on fait 2 boucle if pour affiché soit la premiere fois : avec head -1 ou la derniere fois que la commande est tapée avec tail
