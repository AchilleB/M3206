#!/bin/bash
f="first"
l="last"

if [ $1 = $f ]
	
then
time=$(cat $3 | awk '{print $2}' | grep $2| cut -d ':' -f1 | head -1 )
fi

if [ $1 = $l ]
then
time=$(cat $3 | awk '{print $2}' | grep $2| cut -d ':' -f1 | tail -1 )
fi

date -d @$time


#on créer une variable cat , qui prend en compte les paramètres que l'on rentre en autre la commande et le fichier ici history

#l'option date permet d'affiché la date au format string (-d)


#Ici on fait 2 boucle if pour affiché soit la premiere fois : avec head -1 ou la derniere fois que la commande est tapée avec tail
