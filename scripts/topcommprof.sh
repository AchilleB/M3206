cat /root/M3206/scripts/my_history | awk '{print $2}' | cut -d ';' -f2 | sort | uniq -c| sort -nr | head -$1

#On trie les commandes avec l'option sort par ordre alphabétique
#Head pour gardé le nombre de commandes que l'on veut affiché
#Avec awk on ne garde que la commande qui affiche les commandes les plus utilisés
# cut-d  et -f2  pour indiqué que l'on coupe en fonction de la chaine de caractère et garde ce qui est nécessaire
# awk '{print $2}'  : affiche la deuxiemme colonne de la sortie uniquement

